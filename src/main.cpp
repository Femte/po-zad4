#include <iostream>
#include "Wektor.hh"
#include "Macierz.hh"
#include "UkladRownanLiniowych.hh"
#include "LZespolona.hh"


using namespace std;

/*
 * Tu definiujemy pozostale funkcje.
 * Lepiej jednak stworzyc dodatkowy modul
 * i tam je umiescic. Ten przyklad pokazuje
 * jedynie absolutne minimum.
 */


int main()
{

  char x;
  cin >> x;
  if(x == 'r'){
      SUkladRownanLiniowych<double, ROZMIAR> B;
      SWektor<double, ROZMIAR> C, D;

      cin >> B;

      C = B.obliczuklad(); 
      B.wywtrans(); 

      cout << B;

      D = B.wekbl(C); 
      wyswrozw(C, D); 
  }
  else if (x == 'z')
  {
      SUkladRownanLiniowych<LZespolona, ROZMIAR> B;
      SWektor<LZespolona, ROZMIAR> C, D;

      cin >> B;

      C = B.obliczuklad();
      B.wywtrans();

      cout << B;

      D = B.wekbl(C);
      wyswrozw(C, D);

      return 0;

  }
  else
  {
      cout << endl << "Nieprawidlowa opcja wywolania!" << endl;
      cout << "Dostepne opcje to: 'r'->(rzeczywiste) lub 'z'->(zespolone)" << endl;

      return 1;
  }
}
