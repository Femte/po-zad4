#ifndef LZESPOLONA_HH
#define LZESPOLONA_HH
#include <iostream>

using namespace std;

struct LZespolona {
  double  re;
  double im;


 void operator = (int arg)
  {
      re = arg;
      im = 0;
  }

  void operator = (LZespolona arg)
  {
      re = arg.re;
      im = arg.im;
  }

  bool operator == (int arg)
  {
      if(re == arg && im == 0)
          return true;
      else
          return false;
  }

    bool operator == (LZespolona arg)
    {
        if(re == arg.re && im == arg.im)
            return true;
        else
            return false;
    }

};

ostream& operator << (ostream& StrWyj, LZespolona liczba);

void Wyswietl (LZespolona arg);

LZespolona operator + (LZespolona  Skl1,  LZespolona  Skl2);

LZespolona sprzezenie (LZespolona Skl);

double modul (LZespolona Skl);

LZespolona operator * (LZespolona Skl1, LZespolona Skl2);

LZespolona operator - (LZespolona Skl1, LZespolona Skl2);

LZespolona operator / (LZespolona Skl1, LZespolona Skl2);

LZespolona Wczytaj();

istream& operator >> (istream& StrWej, LZespolona& liczba);

LZespolona operator * (LZespolona Skl1, int b);

#endif
