#ifndef WEKTOR_HH
#define WEKTOR_HH

#include "rozmiar.h"
#include "LZespolona.hh"
#include <iostream>


/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
template <typename STyp, int SWymiar>
class SWektor {
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich pol i metod prywatnych
   */
STyp tab[SWymiar];
  public:
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich metod publicznych
   */ 
STyp daj(int b) const { return tab[b]; }
void wez(STyp z, int p) { tab[p]=z; }
STyp  operator [] (unsigned int Ind) const { return tab[Ind]; }
STyp &operator [] (unsigned int Ind)       { return tab[Ind]; }
};


template <typename STyp, int SWymiar>
std::istream& operator >> (std::istream &Strm, SWektor<STyp, SWymiar> &Wek)
{
    STyp war;
    for(int i=0; i<SWymiar; ++i)
    {
        Strm >> war;
        Wek.wez(war,i); 
    }
    return Strm;
}


template <typename STyp, int SWymiar>
std::ostream& operator << (std::ostream &Strm, const SWektor<STyp, SWymiar> &Wek)
{
    for(int i=0; i<SWymiar; ++i) 
    {
        Strm << Wek.daj(i) << "  "; 
    }
    return Strm;
}

template <typename STyp, int SWymiar>
SWektor<STyp, SWymiar> operator + (SWektor<STyp, SWymiar> Zm1, SWektor<STyp, SWymiar> Zm2)
{
    SWektor<STyp, SWymiar> wek;
    STyp wynik;
    for (int i=0; i<SWymiar; ++i) 
    {
        wynik = Zm1.daj(i) + Zm2.daj(i); 
        wek.wez(wynik,i); 
    }
    return wek;
}

template <typename STyp, int SWymiar>
SWektor<STyp, SWymiar> operator - (SWektor<STyp, SWymiar> Zm1, SWektor<STyp, SWymiar> Zm2)
{
    SWektor<STyp, SWymiar> wek;
    STyp wynik;
    for (int i=0; i<SWymiar; ++i)
    {
        wynik = Zm1.daj(i) - Zm2.daj(i);
        wek.wez(wynik,i);
    }
    return wek;
}

template <typename STyp, int SWymiar>
double operator * (SWektor<STyp, SWymiar> Zm1, SWektor<STyp, SWymiar> Zm2)
{
    double iloczyn, wynik;
    for (int i=0; i<SWymiar; ++i)
    {
        iloczyn = Zm1.daj(i) * Zm2.daj(i);
        wynik+=iloczyn; 
    }
    return wynik;
}

template <typename STyp, int SWymiar>
SWektor<STyp, SWymiar> operator * (SWektor<STyp, SWymiar> Zm1, double Zm2)
{
    SWektor<STyp, SWymiar> wek;
    STyp wynik;
    for (int i=0; i<SWymiar; ++i)
    {
        wynik = Zm1.daj(i) * Zm2;
        wek.wez(wynik, i);

    }
    return wek;
}

template <typename STyp, int SWymiar>
SWektor<STyp, SWymiar> operator / (SWektor<STyp, SWymiar> Zm1, double Zm2)
{
    SWektor<STyp, SWymiar> wek;
    STyp wynik;
    for (int i=0; i<SWymiar; ++i)
    {
        wynik = Zm1.daj(i) / Zm2;
        wek.wez(wynik, i);

    }
    return wek;
}

#endif
