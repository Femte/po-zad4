#ifndef UKLADROWNANLINIOWYCH_HH
#define UKLADROWNANLINIOWYCH_HH

#include <iostream>
#include "Macierz.hh"

/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
template <typename STyp, int SWymiar>
class SUkladRownanLiniowych {
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich pol i metod prywatnych
   */
  SWektor<STyp, SWymiar> wek;
  SMacierz<STyp, SWymiar> mac;
  public:
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich metod publicznych
   */ 
 void wezmU(SWektor<STyp, SWymiar> C, int i) { mac.wezw(C, i); }

 void wezwU(STyp z, int p) { wek[p] = z; }

 STyp dajwU(int b) const { return  wek[b]; }

 SWektor<STyp, SWymiar> dajj(int i) const { return mac.dajw(i); }

 void wywtrans() { mac.transponuj(); }

 SWektor<STyp, SWymiar> obliczuklad();

 SWektor<STyp, SWymiar> wekbl(SWektor<STyp, SWymiar> zz);   
};


/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
template <typename STyp, int SWymiar>
std::istream& operator >> (std::istream &Strm, SUkladRownanLiniowych<STyp, SWymiar> &UklRown)
{
    SWektor<STyp, SWymiar> wek;
    STyp liczba;
    for(int i=0; i<SWymiar; ++i) 
    {
        Strm >> wek;
        UklRown.wezmU(wek, i);
    }

    for(int i=0; i<SWymiar; ++i) 
    {
        Strm >> liczba;
        UklRown.wezwU(liczba, i);

    }

    return Strm;
}

/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
template <typename STyp, int SWymiar>
std::ostream& operator << (std::ostream &Strm, const SUkladRownanLiniowych<STyp, SWymiar> &UklRown)
{
    Strm << std::endl << "Uklad rownan do rozwiazania: " << std::endl << std::endl;
    for(int i=0; i<SWymiar; ++i) 
    {
        Strm << "|  " << UklRown.dajj(i) <<  "| | x_" << i+1 << " |";
        if((i==SWymiar/2)) 
        {
            Strm << " = | " << UklRown.dajwU(i) << " |" << std::endl;
        }
        else
        {
            Strm << "   | " << UklRown.dajwU(i) << " |" << std::endl;
        }

    }
    Strm << std::endl << std::endl;
    return Strm;
}

template <typename STyp, int SWymiar>
SWektor<STyp, SWymiar> SUkladRownanLiniowych<STyp, SWymiar>::obliczuklad ()
{
    STyp x, y;
    SWektor<STyp, SWymiar> wynik, tmpglowny;
    SMacierz<STyp, SWymiar> X;
    int licznikgl = 0, licznikpomoc = 0;

    X = mac; 
    y = X.det(); 
    if (y == 0)
    {
        licznikgl++;
    }
    for (int i = 0; i<ROZMIAR; ++i)
    {
        tmpglowny = mac.dajw(i);
        mac.wezw(wek, i); 
        X = mac; 
        x = X.det(); 
        if(x == 0)
        {
            licznikpomoc++;
        }
        wynik.wez(x/y, i); 
        mac.wezw(tmpglowny, i); 
    }

    if((licznikgl == 1) && (licznikpomoc == SWymiar))
    {
        std::cerr << "Uklad jest nieoznaczony!";
        exit(1);
    }
    else if ((licznikgl == 1) && (licznikpomoc < SWymiar))
    {
        std::cerr << "Uklad jest sprzeczny!";
        exit(1);
    }

    return wynik; 
}

template <typename STyp, int SWymiar>
SWektor<STyp, SWymiar> SUkladRownanLiniowych<STyp, SWymiar>::wekbl(SWektor<STyp, SWymiar> zz)
{
    STyp b;
    b = 0;
    SWektor<STyp, SWymiar> X;
    SMacierz<STyp, SWymiar> B = mac;

    for(int i = 0; i<SWymiar; ++i) 
    {
        for(int j = 0; j<SWymiar; ++j)
        {
            b = b + (B(i,j)*zz[j]); 
        }
        X[i] = b; 
        b = 0;
    }
    return X - wek; 
}

template <typename STyp, int SWymiar>
void wyswrozw(SWektor<STyp, SWymiar> C, SWektor<STyp, SWymiar> B)
{
  std::cout << "Wektor rozwiazan:" << std::endl  << std::endl << "\t     " << C << std::endl << std::endl;
  std::cout << "Wektor bledu:" << std::endl << std::endl << "\tAx-b  =  (  " << B << ")" << std::endl << std::endl;
}

#endif
