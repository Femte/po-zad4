#ifndef MACIERZ_HH
#define MACIERZ_HH

#include "Wektor.hh"
#include <iostream>


/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
template <typename STyp, int SWymiar>
class SMacierz {
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich pol i metod prywatnych
   */
 SWektor<STyp, SWymiar> tab[SWymiar];
  public:
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich metod publicznych
   */    
 SWektor<STyp, SWymiar> dajw(int i) const { return tab[i]; }
 void wezw(SWektor<STyp, SWymiar> C, int i) { tab[i] = C; }
 void transponuj(); 
 STyp  operator () (unsigned int Ind, unsigned int j) const { return tab[Ind][j]; } 
 STyp &operator () (unsigned int Ind, unsigned int j)       { return tab[Ind][j]; }
 STyp det();
};


/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
template <typename STyp, int SWymiar>
std::istream& operator >> (std::istream &Strm, SMacierz<STyp, SWymiar> &Mac)
{
    SWektor<STyp, SWymiar> wek;
    for(int i = 0; i<SWymiar; ++i) 
    {
        Strm >> wek;
        Mac.wezw(wek,i); 
    }
    return Strm;
}

/*
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt 
 */
template <typename STyp, int SWymiar>
std::ostream& operator << (std::ostream &Strm, const SMacierz<STyp, SWymiar> &Mac)
{
    Strm << std::endl;
    for(int i = 0; i<SWymiar; ++i) 
    {
        Strm << "\t" << Mac.dajw(i) << std::endl;
    }
    return Strm;
}

template <typename STyp, int SWymiar>
void SMacierz<STyp, SWymiar>::transponuj()
{
    SWektor<STyp, SWymiar> C, D;
    SMacierz<STyp, SWymiar> mac;

    for(int i=0; i<SWymiar; ++i)
    {
        for(int y=0; y<SWymiar; ++y) 
        {                           
            C = tab[y];
            D[y] = C[i];
        }
        mac.wezw(D, i); 
    }

    for(int b = 0; b<SWymiar; ++b)
    {
        tab[b] = mac.dajw(b); 
    }
}

template <typename STyp, int SWymiar>
STyp SMacierz<STyp, SWymiar>::det()
{
    SMacierz<STyp, SWymiar> mac = (*this);
    STyp elem;
    STyp wynik;

    elem = 0;
    wynik = 1;

    for (int i = 0; i<SWymiar-1; i++)
    {
        for (int j = i+1; j<SWymiar; j++)
        {
            elem = (mac.tab[j][i])*(-1)/mac.tab[i][i];
            for (int k = i; k<=SWymiar; k++)
            {
                mac.tab[j][k] = mac.tab[j][k] + (elem*mac.tab[i][k]);
            }
        }
    }

    for (int l = 0; l<SWymiar; l++)
    {
        wynik = (mac(l, l) * wynik);

    }
    return wynik;
}

#endif
